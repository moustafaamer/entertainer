# Entertainer

Entertainer is a simple web app and REST API application that returns activities from [BoredAPI](https://www.boredapi.com/documentation) based on user price & accessibility tier requirements.

# Getting Started

## Starting the server

Clone repository and navigate to project directory

```
git clone #---
cd entertainer
```

Install packages

```
npm install
```

- `.env` file has been committed to simplify testing. The server port is set to 8000; however, the port is configurable through `.env`

Run server

```
npm run start
```

![POST request sample](screenshots/POST.png)

![GET request sample](screenshots/GET.png)

## Starting client app

Navigate to React app code

```
cd client/app
```

Install packages

```
npm install
```

Note: If the server port was changed from 8000, please update `client/app/.env` to point `REACT_APP_SERVER_URL` to new server domain.

Run app

```
npm start
```
