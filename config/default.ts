require('dotenv').config();

export default {
    port: process.env.PORT || 8000,
    host: 'localhost',
    boredApiDomain: 'http://www.boredapi.com',
}