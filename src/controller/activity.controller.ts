import { Request, Response } from 'express';
import activityService from '../service/activity.service';
import log from "../logger";

class ActivityController {
  public async getActivityHandler(req: Request, res: Response) {
    log.info(req);
    const activity = await activityService.getActivity();
    res.status(200).send(activity);
  }
}

export default new ActivityController();
