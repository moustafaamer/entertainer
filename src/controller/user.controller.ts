import { Request, Response } from 'express';
import userService from '../service/user.service';
import log from "../logger";

class UserController {
  public async addUserHandler(req: Request, res: Response) {
    log.info(req);
    await userService.addUser(req.body);
    res.status(200).send(req.body);
  }
}

export default new UserController();
