// Mock DB
import { IUser } from "../model/user.model";

class DB {
  private userTable: IUser[];

  constructor() {
      this.userTable = [];
  }

  // Assuming we only have user table
  public addUser(user: IUser) {
    this.userTable.push(user); 
  }

  // Assuming last user is the current user
  public getUser(): IUser | undefined {
    return this.userTable[this.userTable.length - 1];
  }
}

export default new DB();