import { Request, Response, NextFunction } from "express";
import { AccessibilityTier, PriceTier } from "../model/activity.model";

class UserMiddleware {
  
  public async ValidateRequiredUserBodyFields(req: Request, res: Response, next: NextFunction) {
    if (req.body && req.body.name && req.body.accessibility && req.body.price && Object.keys(req.body).length === 3) {
        next();
    } else {
        res.status(400).send({ error: 'Body validation error, must have fields: [name, accessibility, price] only' });
    }
  }

  public async ValidateUserBodyTypes(req: Request, res: Response, next: NextFunction) {
    const accessibilityTiers = Object.values(AccessibilityTier);
    const priceTiers = Object.values(PriceTier);
    if (!accessibilityTiers.includes(req.body.accessibility)) {
        res.status(400).send({ error: `accessibility must be one of [${accessibilityTiers}]`});
    } else if (!priceTiers.includes(req.body.price)) {
        res.status(400).send({ error: `price must be one of [${priceTiers}]`});
    } else {
        next();
    }
  }
}

export default new UserMiddleware();