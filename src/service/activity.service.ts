import axios from "axios";
import { IBoredApiResponse, IActivity, AccessibilityTier, PriceTier, IAccessibilityRange, IPriceRange } from "../model/activity.model";
import db from "../db/connect";
import config from "config";
import log from "../logger";

const boredApiDomain = config.get('boredApiDomain') as string;

class ActivityService {

  public async getActivity(): Promise<IActivity> {
    try {
      const currentUser = db.getUser();
      log.debug(currentUser);
      let boredApiResponse: IBoredApiResponse;
      if (currentUser) {
        boredApiResponse = await this.getUserActivity(currentUser.accessibility, currentUser.price);
      } else {
        boredApiResponse = await this.getGeneralActivity();
      }
      
      log.info(boredApiResponse);
      const activity:IActivity = {
        ...boredApiResponse,
        accessibility: this.getAccessibilityTier(boredApiResponse.accessibility),
        price: this.getPriceTier(boredApiResponse.price)
      };
      log.info(activity);
      return activity;
    } catch (error: any) {
      log.error(error);
      throw new Error(error);
    }
  }

  private async getUserActivity(accessibility: AccessibilityTier, price: PriceTier): Promise<IBoredApiResponse> {
    const accessibilityRange = this.getAccessibilityRange(accessibility);
    const priceRange = this.getPriceRange(price);
    log.debug(accessibilityRange);
    log.debug(priceRange);
    const queryParams =
    `minprice=${priceRange.min}&` +
    `maxprice=${priceRange.max}&` +
    `minaccessibility=${accessibilityRange.min}&` +
    `maxaccessibility=${accessibilityRange.max}`;
    return await (await axios.get(`${boredApiDomain}/api/activity?${queryParams}`)).data; 
  }

  private async getGeneralActivity(): Promise<IBoredApiResponse> {        
    return await (await axios.get(`${boredApiDomain}/api/activity`)).data;
  }

  private getAccessibilityTier(accessibility: number): AccessibilityTier {
    if (accessibility <= 0.25) {
      return AccessibilityTier.High;
    } else if (accessibility <= 0.75) {
      return AccessibilityTier.Medium;
    } else {
      return AccessibilityTier.Low;
    }
  }

  private getPriceTier(price: number): PriceTier {
    if (price == 0) {
      return PriceTier.Free;
    } else if (price <= 0.5) {
      return PriceTier.Low;
    } else {
      return PriceTier.High;
    }
  }

  private getAccessibilityRange(accessibilityTier: AccessibilityTier): IAccessibilityRange {
    const accessibilityRange:IAccessibilityRange = {
      min: 0,
      max: 0
    };
    if (accessibilityTier === AccessibilityTier.High) {
      accessibilityRange.min = 0;
      accessibilityRange.max = 0.25; 
    } else if (accessibilityTier === AccessibilityTier.Medium) {
      accessibilityRange.min = 0.26;
      accessibilityRange.max = 0.75;
    } else {
      accessibilityRange.min = 0.76;
      accessibilityRange.max = 1.0;
    }
    return accessibilityRange;
  }

  private getPriceRange(priceTier: PriceTier): IPriceRange {
    const priceRange: IPriceRange = {
      min: 0,
      max: 0
    };
    if (priceTier === PriceTier.Low) {
      priceRange.min = 0.1;
      priceRange.max = 0.5;
    } else if (priceTier === PriceTier.High) {
      priceRange.min = 0.6;
      priceRange.max = 1;
    } else {
      priceRange.min = 0;
      priceRange.max = 0;
    }
    return priceRange;
  }
}
export default new ActivityService();