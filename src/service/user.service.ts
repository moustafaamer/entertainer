import { IUser } from "../model/user.model";
import db from "../db/connect";
import log from "../logger";

class UserService {
  public async addUser(user: IUser) {
    try {
      await db.addUser(user);
    } catch (error: any) {
      log.error(error);
      throw new Error(error);
    }
  }
}

export default new UserService();